from django.db import models

from uploaders.models import Member

# Create your models here.
class File(models.Model):
    filename = models.CharField(max_length=200)
    file = models.FileField(upload_to ='archivos/')
    description = models.TextField(blank=True)
    show_public = models.BooleanField(default=False)
    upload_at = models.DateField(auto_now=True)
    upload_by = models.ForeignKey(Member)

    def __str__(self):
        return "%s" % self.filename
