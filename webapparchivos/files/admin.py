from django.contrib import admin

from files.models import File
# Register your models here.
@admin.register(File)
class AdminFile(admin.ModelAdmin):
    list_display = ('filename','file','upload_by','show_public','upload_at',)