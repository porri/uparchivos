from django.forms import forms, ModelForm
from django.contrib.auth.models import User
from files.models import File

from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

class FileAddForm(ModelForm):

    class Meta:
        model = File
        fields = (
            #'member',
            'filename',
            'file',
            'show_public',
            'description',
            #'upload_at',
            #'upload_by',

        )

