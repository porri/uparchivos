from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import FormView,DeleteView,ListView

from files.forms import FileAddForm
from files.models import File
from uploaders.models import Member
# Create your views here.

class SocialListFile(ListView):
    model = File
    queryset = File.objects.filter(show_public=True)
    template_name = "SocialListFile.html"
    paginate_by = 15

@method_decorator(login_required, name='dispatch')
class UserListFile(ListView):
    model = File
    template_name = "UserListFile.html"

    def get_context_data(self, **kwargs):
        queryset = File.objects.filter(upload_by__member_id__exact = self.request.user.member.member_id)
        context = super(UserListFile, self).get_context_data(**kwargs)
        context['archivos']=queryset

        return context


@method_decorator(login_required, name='dispatch')
class AddFile(FormView):
    model = File
    form_class = FileAddForm
    template_name = "addnewfile.html"
    success_url = reverse_lazy('HomeUser')
    #success_url = reverse_lazy('FileList')

    def form_valid(self, form):

        fe = form.save(commit=False)
        self.object = fe
        fe.upload_by= Member.objects.get( member_id = self.request.user.member.member_id)
        fe.save()

        return HttpResponseRedirect(self.get_success_url())