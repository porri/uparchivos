from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from files.views import AddFile,SocialListFile

urlpatterns = [
    url(r'^newfile/$', AddFile.as_view(), name='NewFile'),
    url(r'^listfilesShow/$', SocialListFile.as_view(), name='SocialListFile'),
]