from .base import *

TEMPLATE_DEBUG = True

THUMBNAIL_DEBUG = True

THUMBNAIL_PRESERVE_FORMAT=True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'l==bi-c=!6_f!tq=9(jl5q#0m495_%emz*+g7!o00iw%50u7&g'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
            'debug_toolbar',
        ]

MIDDLEWARE += [
            'debug_toolbar.middleware.DebugToolbarMiddleware',
        ]

STATICFILES_DIRS = [
    BASE_DIR + '/../static',
]

# Conf para scar la ip interna del docker que funcione debug toolbar
import subprocess

route = subprocess.Popen(('ip', 'route'), stdout=subprocess.PIPE)
network = subprocess.check_output(
    ('grep', '-Po', 'src \K[\d.]+\.'), stdin=route.stdout).decode().rstrip()
route.wait()
network_gateway = network + '1'
INTERNAL_IPS = [network_gateway]
print(INTERNAL_IPS)
