from django.contrib import admin
from uploaders.models import Member,Friend
# Register your models here.

@admin.register(Member)
class AdminMember(admin.ModelAdmin):
    list_display = ("member","nombre","apellido1","apellido2","email","avatar","creado",)

@admin.register(Friend)
class AdminFriend(admin.ModelAdmin):
    list_display = ('from_member','to_member','active',)

