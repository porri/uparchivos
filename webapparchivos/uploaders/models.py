from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.utils import timezone
"""
datos extra a User de django
"""
class Member(models.Model):
    member= models.OneToOneField(User)
    nombre = models.CharField(max_length=200)
    email = models.EmailField(max_length=400)
    apellido1 = models.CharField(max_length=200,blank=True)
    apellido2 = models.CharField(max_length=200,blank=True)
    avatar = models.ImageField(upload_to='avatares/', blank=True)
    creado = models.DateField("Creado",auto_now_add=True)

    def __str__(self):
        return "%s" % self.member.username

class Friend(models.Model):
    from_member = models.ForeignKey(Member, related_name='Friends')
    to_member = models.ForeignKey(Member, related_name='Friends_of')
    active = models.BooleanField(default=False)
