from django.forms import forms, ModelForm, inlineformset_factory, formset_factory
from django.contrib.auth.models import User
from .models import Member
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

class MemberForm(ModelForm):

    class Meta:
        model = Member
        fields = (
            #'member',
            'nombre',
            'apellido1',
            'apellido2',
            'email',
            #'club',
            #'dni',
            #'edad',
            #'telefono',
            'avatar',
        )


#inline
MemberFormSet = inlineformset_factory(User, Member,form=MemberForm, can_delete=False)