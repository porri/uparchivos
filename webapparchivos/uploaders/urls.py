from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .views import LoginView, LogoutView , NewMemberView

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='Login'),
    url(r'^logout/$', login_required(LogoutView.as_view()), name='Logout'),
    url(r'^newmember/$', NewMemberView.as_view(), name='NewUploader'),
]