from django.apps import AppConfig


class UploadersConfig(AppConfig):
    name = 'uploaders'
