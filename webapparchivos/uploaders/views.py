from django.shortcuts import render
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import FormView, RedirectView, CreateView, ListView, TemplateView
from uploaders.forms import MemberFormSet
from uploaders.models import Member
# Create your views here.
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required



class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = 'login.html'
    success_url = reverse_lazy('Home')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        username = self.request.POST['username']
        password = self.request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(self.request, user)
            #self.request.session['memberid'] = user.id
            #self.request.session['nombreuser'] = user.username

        return super(LoginView, self).form_valid(form)

"""
    Vista de logout
"""
@method_decorator(login_required, name='dispatch')
class LogoutView(RedirectView):
    pattern_name = 'Login'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

"""
    Vista para dar de alta usuarios
"""
class NewMemberView(FormView):
    form_class = UserCreationForm
    template_name = 'newuploader.html'
    success_url = reverse_lazy('Home')


    def get(self, request, *args, **kwargs):
        #self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        member_form = MemberFormSet()

        return self.render_to_response(
            self.get_context_data(form=form, member_form=member_form)
        )

    def post(self, request, *args, **kwargs):
        #self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        member_form = MemberFormSet(self.request.POST,self.request.FILES)

        if (form.is_valid() and member_form.is_valid()):
            return self.form_valid(form, member_form)
        else:
            return self.form_invalid(form, member_form)

    def form_valid(self, form, member_form):
        fuser = form.save()
        fuser.save()
        member_form.instance = fuser
        member_form.save()

        return super(NewMemberView, self).form_valid(form)


    def form_invalid(self, form, member_form):
        return self.render_to_response(
            self.get_context_data(form=form, member_form=member_form)
        )

# Vista de inicio de laº web (Home)
@method_decorator(login_required, name='dispatch')
class HomeView(ListView):

    template_name = 'home.html'
